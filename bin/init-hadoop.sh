#!/bin/bash

. config.sh

eval "$(boot2docker shellinit)"

bash hadoop-master-start.sh
bash hadoop-slave-start.sh

sudo hdfs dfs -fs hdfs://${HDFS_IP}/ -put ~/Downloads/airlines.csv /airlines.csv