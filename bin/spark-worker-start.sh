#!/bin/bash

. config.sh

$SPARK_HOME/bin/spark-class org.apache.spark.deploy.worker.Worker spark://${SPARK_MASTER_IP}:7077 -c 1 -m 2g