Installation
============

Ubuntu 12.04 or lower
---------------------

sudo apt-get install linux-image-generic-lts-trusty

sudo reboot

wget -qO- https://get.docker.com/ | sh

Ubuntu 14.04
------------

wget -qO- https://get.docker.com/ | sh

Running
=======

docker pull secaglobal/spark-demo

docker run -d -e SPARK_MASTER_URL=<SPARK_URL> --name spark-demo --net=host  secaglobal/spark-demo