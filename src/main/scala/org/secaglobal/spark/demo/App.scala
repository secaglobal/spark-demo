package org.secaglobal.spark.demo

import org.springframework.boot.SpringApplication

/**
 * Hello world!
 *
 */
object App {
  def main(args: Array[String]) {
    SpringApplication.run(classOf[Configuration]);
  }
}


