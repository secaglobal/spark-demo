package org.secaglobal.spark.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.{RestController, ResponseBody, RequestMapping}

/**
 * @author Sergey Levandovskiy <levandovskiy.s@gmail.com>
 */
@SpringBootApplication
@RestController
class Configuration {
  @RequestMapping(Array("/"))
  @ResponseBody
  def countWord: String = "OK"
}
