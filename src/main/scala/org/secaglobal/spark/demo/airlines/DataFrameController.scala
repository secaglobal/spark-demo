package org.secaglobal.spark.demo.airlines

import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, ResponseBody, RestController}


@RestController
@RequestMapping(Array("/"))
class DataFrameController {
  @Autowired
  private val sc: SparkContext = null;

  @Autowired
  private val sqlc: SQLContext = null;

  @RequestMapping(Array("/data-frame"))
  @ResponseBody
  def dfLikeCountWord: String = {
    val startAt = System.nanoTime()

    val df = sqlc.table("Events")
    val result = df.filter(df("from").equalTo("New York")).count()

    "Lines number: %d. Spent time: %f".format(result, (System.nanoTime() - startAt) / 1000000000.0)
  }
}
