package org.secaglobal.spark.demo.airlines

import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, ResponseBody, RestController}


@RestController
@RequestMapping(Array("/"))
class DistanceController {
  @Autowired
  private val sc: SparkContext = null;

  @Autowired
  private val sqlc: SQLContext = null;

  @RequestMapping(Array("/distance"))
  @ResponseBody
  def countWord: String = {
    val startAt = System.nanoTime()

    val df = sqlc.table("Events")

    val grouped = sqlc.sql("select sum(distance) from Events group by id")
    val count = grouped.count()
    val first = grouped.first().getDouble(0)

    "Total races: %d, First race distance: %.0f, Spent time: %f".format(count, first, (System.nanoTime() - startAt) / 1000000000.0)
  }
}
