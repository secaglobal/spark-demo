package org.secaglobal.spark.demo.airlines


import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkContext, SparkConf}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RestController, ResponseBody, RequestMapping}


@RestController
@RequestMapping(Array("/"))
class LoadController {
  @Autowired
  private val sc: SparkContext = null;

  @Autowired
  private val sqlc: SQLContext = null;

  import sqlc.implicits._

  @RequestMapping(Array("/load"))
  @ResponseBody
  def loadTable: String = {
    val startAt = System.nanoTime()
    //val logFile = "hdfs://192.168.1.6/airlines.csv"
    val logFile = "file:///Users/lev/Downloads/airlines.csv"

    val logData = sc.textFile(logFile, 2).zipWithIndex().filter(_._2 > 0).map {
      case (line, indx) =>
        val c = line.replace("\"", "").split(",")
        Event(c(0).toLong, c(4).toInt, c(12), c(20), c(29).toDouble)
    }.toDF().cache()

    logData.count()

    logData.registerTempTable("Events")
    "Loading time: %f sec".format((System.nanoTime() - startAt) / 1000000000.0)
  }
}

case class Event(id: Long, year: Int, from: String, to: String, distance: Double)
