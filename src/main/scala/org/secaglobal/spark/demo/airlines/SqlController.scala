package org.secaglobal.spark.demo.airlines

import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, ResponseBody, RestController}


@RestController
@RequestMapping(Array("/"))
class SqlController {
  @Autowired
  private val sc: SparkContext = null;

  @Autowired
  private val sqlc: SQLContext = null;

  @RequestMapping(Array("/sql"))
  @ResponseBody
  def sqlLikeCountWord: String = {
    val startAt = System.nanoTime()

    val result = sqlc.sql("select * from Events where `from` = 'New York'").count()

    "Lines number: %d. Spent time: %f".format(result, (System.nanoTime() - startAt) / 1000000000.0)
  }
}
