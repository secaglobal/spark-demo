package org.secaglobal.spark.demo.beans

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.{Scope, Bean, Configuration}

@Configuration
class SparkBeans {

  @Bean
  @Scope
  def sparkContext: SparkContext = {
    val conf = new SparkConf().setAppName("Simple Application")
      .setMaster("spark://192.168.1.103:7077")
      .set("spark.executor.memory", "2g")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    val sc = new SparkContext(conf)
    sc.addJar(System.getProperty("spark.jar"))
    sc
  }

  @Bean
  @Scope
  @Autowired
  def sqlSparkContext(sparkContext: SparkContext): SQLContext = {
    new SQLContext(sparkContext)
  }
}
